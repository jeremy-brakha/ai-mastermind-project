PROLOG = swipl
SOURCE = mastermind.pl
TESTS = test_mastermind.pl

.PHONY: all test clean

all:
	@echo "Utilisez 'make test' pour lancer les tests."

test: $(SOURCE) $(TESTS)
	$(PROLOG) -s $(TESTS) -g "run_tests, halt"
