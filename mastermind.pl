% MasterMind Game

% Question 1
color(blue).
color(red).
color(green).
color(yellow).
color(white).
color(black).

% Question 2
combination([]).
combination([E|L]) :-
    color(E), % E est une couleur
    combination(L). % On vérifie pour le reste de la liste

% Exercice 1 : Calcul des réponses
% Question 3
well_placed([], [], 0).
well_placed([L1| L1_tail], [L2|L2_tail], R) :- % On vérifie si les deux premières couleurs sont les mêmes
    L1 = L2, % Si les deux couleurs sont les mêmes
    well_placed(L1_tail, L2_tail, N), % On vérifie pour le reste de la liste
    R is N + 1. % On incrémente le nombre de couleurs bien placées
well_placed([L1| L1_tail], [L2|L2_tail], R) :- % On vérifie si les deux premières couleurs sont différentes
    L1 \= L2, % Si les deux couleurs sont différentes
    well_placed(L1_tail, L2_tail, R). % On vérifie pour le reste de la liste

% Question 4
remove_well_placed([], [], [], []).
remove_well_placed([L1| L1_tail], [L2|L2_tail], NP, NS) :- % On vérifie si les deux premières couleurs sont les mêmes
    L1 = L2, % Si les deux couleurs sont les mêmes
    remove_well_placed(L1_tail, L2_tail, NP, NS). % On vérifie pour le reste de la liste
remove_well_placed([L1| L1_tail], [L2|L2_tail], [L1|NP], [L2|NS]) :- % On vérifie si les deux premières couleurs sont différentes
    L1 \= L2, % Si les deux couleurs sont différentes
    remove_well_placed(L1_tail, L2_tail, NP, NS). % On vérifie pour le reste de la liste

% Question 5
mem_remove(X, [X|L], L). % On vérifie si X est dans la liste
mem_remove(X, [J|L], [J|Q]) :- % On vérifie si X n'est pas dans la liste
    mem_remove(X, L, Q). % On vérifie pour le reste de la liste

% Question 6
wrong_placed([], _, 0). % Si la liste est vide, il n'y a pas de couleurs mal placées
wrong_placed([L1| L1_tail], S, R) :-
    mem_remove(L1, S, S1), % On vérifie si la couleur est dans la solution
    wrong_placed(L1_tail, S1, N), % On vérifie pour le reste de la liste
    R is N + 1. % On incrémente le nombre de couleurs mal placées
wrong_placed([L1| L1_tail], S, R) :-
    \+ mem_remove(L1, S, _), % On vérifie si la couleur n'est pas dans la solution
    wrong_placed(L1_tail, S, R). % On vérifie pour le reste de la liste

% Question 7
answer(P, S, G, B) :-
    well_placed(P, S, WP1),           % Nombre de couleurs bien placées
    remove_well_placed(P, S, NP, NS),% Proposition et solution sans les couleurs bien placées
    wrong_placed(NP, NS, WP2),        % Nombre de couleurs mal placées
    G is WP1,
    B is WP2.


% Exercice 2 : Suggestion des propositions
% Question 8
check_proposition(_, []). % Si la liste est vide, on arrête la vérification
check_proposition(P, [H1, H2, H3|T]) :-
    answer(P, H1, H2, H3), % On vérifie que la proposition est compatible avec la proposition précédente
    check_proposition(P, T). % On vérifie pour le reste de la liste

% Question 9
% Pour répondre à la question, on va avoir besoin de générer des propositions
generate_combination(0, []). % Si N est nul, on arrête la génération
generate_combination(N, [H|T]) :-
    N > 0, % on vérifie que N est positif
    color(H), % on génère une couleur
    N1 is N - 1, % on décrémente N
    generate_combination(N1, T). % on génère la suite de la proposition

suggest(P, L) :-
    length(P, N), % déterminer la longueur de la combinaison
    generate_combination(N, P), % générer une combinaison
    check_proposition(P, L), % vérifier que la combinaison est compatible avec les propositions précédentes
    !. % arrêter la recherche dès qu'une combinaison est trouvée

% Question 10
% Voici deux exemples qui renverront une nouvelle suggestion
% suggest(P, [[yellow, white, blue], 1, 1]).
% suggest(P, [[yellow, white, white, black], 0, 0, [blue, blue, green, green], 2, 1]).

% Exercice 3
% Question 11
% extract_proposals([], []).
% extract_proposals([P, _, _|T], [P|Result]) :-
%     extract_proposals(T, Result).

play([], S, R) :-
    length(S, Len), % déterminer la longueur de la combinaison
    generate_combination(Len, Comb), % générer une combinaison
    answer(Comb, S, G, B), % vérifier que la combinaison est compatible avec la solution
    append([], [Comb, G, B], NewL), % ajouter la combinaison à la liste
    play(NewL, S, R). % recommencer

play(L, S, R) :-
    suggest(P, L), % générer une nouvelle proposition
    (P = S -> % vérifier si la proposition est la solution
        length(S, Len), % déterminer la longueur de la combinaison
        answer(P, S, Len, 0), % vérifier que la combinaison est compatible avec la solution
        append(L, [P, Len, 0], R) % ajouter la combinaison à la liste
        % extract_proposals(FullR, R)
    ;
    answer(P, S, G, B), % vérifier que la combinaison est compatible avec la solution
    append(L, [P, G, B], NewL), % ajouter la combinaison à la liste
    play(NewL, S, R) % recommencer
    ).

% Question 12
% Cas de base: Si la liste est vide, il n'y a pas de solution et N = 0.
score([], 0). 
score(S, N) :-
    play([], S, R), % On joue le jeu avec la solution S
    length(R, L), % On détermine la longueur de la liste
    N is L // 3. % On divise par 3 pour avoir le nombre de coups car il y a la proposition, le nombre de couleurs bien placées et le nombre de couleurs mal placées

% Question 13
% On va générer toutes les combinaisons possibles de couleurs
color_combinations(Combinations) :-
    findall([C1, C2, C3, C4], (color(C1), color(C2), color(C3), color(C4)), Combinations).

% On va parcourir la liste des combinaisons et trouver la combinaison qui a le meilleur score
find_max_score([], MaxScore, CurrentList, MaxScore, CurrentList).
find_max_score([Head|Tail], MaxScore, CurrentList, FinalMaxScore, FinalList) :-
    score(Head, Score),
    (Score > MaxScore ->
        find_max_score(Tail, Score, [Head], FinalMaxScore, FinalList)
    ; 
     Score =:= MaxScore ->
        append(CurrentList, [Head], NewList),
        find_max_score(Tail, MaxScore, NewList, FinalMaxScore, FinalList)
    ;
    find_max_score(Tail, MaxScore, CurrentList, FinalMaxScore, FinalList)
    ).

% Renvoyer le score maximal et les combinaisons qui le donnent
max_score_combinations(MaxScore, Combinations) :-
    color_combinations(AllCombinations),
    find_max_score(AllCombinations, 0, [], MaxScore, Combinations).

% Le score maximal est 9, et la combinaison qui le donne est:
% [[black, yellow, green, white]].

% Question 14
% Filtrer les combinaisons en se basant sur l'historique des suppositions et leur score
profitable_combinations(_, [], []).
profitable_combinations(H, [C|Combinations], [C|F]) :-
    check_proposition(C, H),
    profitable_combinations(H, Combinations, F).
profitable_combinations(H, [C|Combinations], F) :-
    \+ check_proposition(C, H),
    profitable_combinations(H, Combinations, F).

% Permet de calculer le score d'une proposition par rapport à une liste de solutions possibles
score_variant(_, [], 0, 0).
score_variant(P, [S|SS], N, M1) :-
    answer(P, S, G, B),
    T is G + B,
    score_variant(P, SS, N1, M2),
    (T > M2 -> 
        M1 is T
    ; 
    M1 is M2),
    N is N1 + 1.

% Permet de choisir la meilleure proposition parmi une liste
best_proposition(_, [], _, B, B).
best_proposition(Props, [P|PS], MScore, CB, B) :-
    score_variant(P, Props, _, Score),
    (Score < MScore ->
        best_proposition(Props, PS, Score, P, B)
    ;
    best_proposition(Props, PS, MScore, CB, B)
    ).

% Permet de générer une proposition avec deux couleurs répétées à la suite
generate_double_color_proposition([C1, C1, C2, C2]) :-
    color(C1),
    color(C2),
    C1 \= C2.

resolution(S, H) :-
    generate_double_color_proposition(P),
    resolution(S, H, P).

resolution(S, H, P) :-
    answer(P, S, G, B),
    (G =:= 4 -> % Si la proposition est la solution
        append(H, [P, G, B], NH),
        writeln(NH)
    ;
    % On met à jour l'historique avec proposition et son score
    append(H, [P, G, B], NH),
    % On génère toutes les combinaisons possibles
    color_combinations(Combinations),
    % On filtre les combinaisons avec plus-value en se basant sur l'historique
    profitable_combinations(NH, Combinations, AP),
    % On choisit la meilleure proposition parmi les combinaisons filtrées
    best_proposition(AP, AP, 99999, [], BP),
    % On recommence avec la nouvelle proposition
    resolution(S, NH, BP)
    ).
