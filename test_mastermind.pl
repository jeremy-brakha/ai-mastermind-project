:- use_module(library(plunit)).
:- consult('mastermind.pl').

:- begin_tests(mastermind).

test(combination) :-
    combination([blue, green, red]),
    combination([blue, green, red, yellow]),
    not(combination([blue, green, red, yellow, orange])).

test(well_placed) :-
    well_placed([blue, green, red], [blue, green, red], 3),
    well_placed([blue, green, red], [red, blue, green], 0),
    well_placed([blue, green, red], [blue, green, yellow], 2),
    not(well_placed([blue, green], [blue, green], 3)).

test(remove_well_placed) :-
    remove_well_placed([yellow, green, red], [yellow, green, red], [], []),
    remove_well_placed([yellow, green, red], [blue, green, red], [yellow], [blue]).

test(mem_remove) :-
    mem_remove(green, [blue, green, red],  [blue, red]),
    mem_remove(green, [blue, green, red, green, yellow], [blue, red, green, yellow]).

test(wrong_placed) :-
    wrong_placed([blue, green, red], [yellow, blue, white], 1),
    wrong_placed([blue, green, red, green, yellow], [blue, red, white, yellow, green], 4).

test(answer) :-
    answer([blue, green, red], [blue, red, green], 1, 2).

test(check_proposition) :-
    not(check_proposition([blue, blue, blue, green], [[yellow, white, white, black], 0, 0, [blue, blue, green, green], 2, 1])),
    check_proposition([yellow, green, white], [[yellow, white, blue], 1, 1]).

test(suggest) :-
    suggest([blue, green, red, green], [[yellow, white, white, black], 0, 0, [blue, blue, green, green], 2, 1]),
    not(suggest([blue, green, white, green], [[yellow, white, white, black], 0, 0, [blue, blue, green, green], 2, 1])),
    suggest([yellow, red, white], [[yellow, white, blue], 1, 1]).

test(play) :-
    play([], [red, yellow], [[blue,blue],0,0,[red,red],1,0,[red,green],1,0,[red,yellow],2,0]),
    play([[yellow, white, blue, blue], 0, 2], [blue, blue, green, black], [[yellow,white,blue,blue],0,2,[blue,blue,red,red],2,0,[blue,blue,green,green],3,0,[blue,blue,green,black],4,0]).

test(score) :-
    score([green, yellow, yellow, green], 6).

test(max_score_combinations) :-
    max_score_combinations(9, [[white,black,white,yellow],[black,yellow,green,white],[black,white,white,white],[black,white,white,black],[black,black,white,yellow],[black,black,black,white]]).

test(resolution) :-
    resolution([blue, green, red, white], [[white,white,black,black],0,1,[blue,blue,blue,white],2,0,[blue,red,red,white],3,0,[blue,red,green,white],2,2,[blue,green,red,white],4,0], [red, red, blue, blue]).

:- end_tests(mastermind).
